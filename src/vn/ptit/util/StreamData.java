package vn.ptit.util;

public class StreamData {

    public enum Type {
        CONNECT_SERVER,
        LOGIN,
        LOGOUT, 
        LIST_ONLINE, 
        CHAT_ROOM, 
        UNKNOW_TYPE, 
    }

    public static Type getType(String typeName) {
        Type result = Type.UNKNOW_TYPE;

        try {
            result = Enum.valueOf(StreamData.Type.class, typeName);
        } catch (Exception e) {
            System.err.println("Unknow type: " + e.getMessage());
        }

        return result;
    }

    public static Type getTypeFromData(String data) {
        String typeStr = data.split(";")[0];
        return getType(typeStr);
    }
}
