/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vn.ptit.controller;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;

/**
 *
 * @author Cuong Pham
 */
public class SocketHandler {

    public Socket socket;
    public DataInputStream dataInputStream;
    public DataOutputStream dataOutputStream;
    
    public SendMessage sendMessage;
    public ReadMessage readMessage;

    public boolean connect(String addr, int port) {
        try {
            socket = new Socket(addr, port);
            dataInputStream = new DataInputStream(socket.getInputStream());
            dataOutputStream = new DataOutputStream(socket.getOutputStream());
            
            sendMessage = new SendMessage(dataOutputStream);
            sendMessage.start();
            
            readMessage = new ReadMessage(dataInputStream);
            readMessage.start();
            
            return true;
        } catch (IOException e) {
            return false;
        }

    }

}
