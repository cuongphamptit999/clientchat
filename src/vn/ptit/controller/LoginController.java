/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vn.ptit.controller;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.xml.crypto.dsig.spec.C14NMethodParameterSpec;
import vn.ptit.StartClient;
import vn.ptit.view.LoginView;

/**
 *
 * @author Cuong Pham
 */
public class LoginController {
    private LoginView loginView;

    public LoginController(LoginView loginView) {
        this.loginView = loginView;
        this.loginView.addListener(new LoginListener());
    }
    
    class LoginListener implements ActionListener{

        @Override
        public void actionPerformed(ActionEvent e) {
            if(e.getSource() == loginView.getjButton1()){
                String username = loginView.getjTextField1().getText();
                StartClient.socketHandler.sendMessage.login(username);
            }
        }
        
    }
}
