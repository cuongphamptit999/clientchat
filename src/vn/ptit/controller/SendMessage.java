/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vn.ptit.controller;

import java.io.DataOutputStream;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import vn.ptit.util.StreamData;

/**
 *
 * @author Cuong Pham
 */
public class SendMessage extends Thread{
    DataOutputStream dataOutputStream;

    public SendMessage(DataOutputStream dataOutputStream) {
        this.dataOutputStream = dataOutputStream;
    }
    
    public void connectServer () {
        sendData(StreamData.Type.CONNECT_SERVER.name());
    }
    
    public void login(String username) {
        String data = StreamData.Type.LOGIN.name() + ";" + username;
        // send data
        sendData(data);
    }
    
    public void logout() {
        String data = StreamData.Type.LOGOUT.name();
        // send data
        sendData(data);
    }
    
    public void listOnline(){
        sendData(StreamData.Type.LIST_ONLINE.name());
    }
    
    public void sendData(String data) {
        try {
            dataOutputStream.writeUTF(data);

        } catch (IOException ex) {
            System.out.println(ex);
        }
    }
    
    public void chatRoom(String chatMsg) {
        sendData(StreamData.Type.CHAT_ROOM.name() + ";" + chatMsg);
    }
    
}
