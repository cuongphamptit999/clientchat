/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vn.ptit.controller;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.DefaultListModel;
import vn.ptit.StartClient;
import vn.ptit.model.ChatItem;
import vn.ptit.view.ChatView;

/**
 *
 * @author Cuong Pham
 */
public class ChatController {

    private ChatView chatView;

    public ChatController(ChatView chatView) {
        this.chatView = chatView;
        this.chatView.addListener(new ChatListener());
    }

    public void setListUser(List<String> listUsername) {
        DefaultListModel modelUser = new DefaultListModel();
        chatView.getjList1().setModel(modelUser);
        modelUser.removeAllElements();
        for (String username : listUsername) {
            modelUser.addElement(username);
        }
    }

    public void setTextUser(String username) {
        chatView.getjLabel2().setText(username);
    }

    public void addChat(ChatItem c) {
        chatView.getjTextArea1().append(c.toString() + "\n");
    }

    class ChatListener implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent e) {
            if (e.getSource() == chatView.getjButton2()) {
                StartClient.socketHandler.sendMessage.listOnline();
            }
            if (e.getSource() == chatView.getjButton1()) {
                String message = chatView.getjTextField1().getText();
                StartClient.socketHandler.sendMessage.chatRoom(message);
                chatView.getjTextField1().setText("");
            }

            if (e.getSource() == chatView.getjButton3()) {
                StartClient.socketHandler.sendMessage.logout();
            }
        }

    }

}
