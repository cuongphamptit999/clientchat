/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vn.ptit.controller;

import com.sun.security.ntlm.Client;
import java.io.DataInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import vn.ptit.StartClient;
import vn.ptit.model.ChatItem;
import vn.ptit.util.StreamData;

/**
 *
 * @author Cuong Pham
 */
public class ReadMessage extends Thread {

    private DataInputStream dataInputStream;
    private String loginUsername = null;

    public ReadMessage(DataInputStream dataInputStream) {
        this.dataInputStream = dataInputStream;
    }

    @Override
    public void run() {
        while (true) {
            try {
                String received = dataInputStream.readUTF();
                StreamData.Type type = StreamData.getTypeFromData(received);
                switch (type) {
                    case LOGIN:
                        onReceiveLogin(received);
                        break;
                    case CONNECT_SERVER:
                        onConnectServer(received);
                        break;
                    case LIST_ONLINE:
                        onReceiveListOnline(received);
                        break;
                    case CHAT_ROOM:
                        onReceiveChatRoom(received);
                        break;
                    case LOGOUT:
                        onReceiveLogout(received);
                        break;
                }
            } catch (IOException ex) {
                System.out.println(ex);
            }
        }

    }

    private void onReceiveLogin(String received) {
        // get status from data
        String[] splitted = received.split(";");
        String status = splitted[1];

        if (status.equals("failure")) {
            JOptionPane.showMessageDialog(StartClient.loginView, "Đăng nhập thất bại, username đã tồn tại", "Lỗi", JOptionPane.ERROR_MESSAGE);

        } else if (status.equals("success")) {
            this.loginUsername = splitted[2];
            System.out.println(loginUsername);
            JOptionPane.showMessageDialog(StartClient.loginView, "Đăng nhập thành công", "Thành công", JOptionPane.INFORMATION_MESSAGE);
            StartClient.openScene(StartClient.SceneName.CHATROOM);
            StartClient.closeScene(StartClient.SceneName.LOGIN);
            StartClient.chatController.setTextUser(loginUsername);
            StartClient.socketHandler.sendMessage.listOnline();
            
        }
    }

    private void onConnectServer(String received) {
        StartClient.closeScene(StartClient.SceneName.CONNECTSERVER);
        StartClient.openScene(StartClient.SceneName.LOGIN);
    }

    private void onReceiveListOnline(String received) {
        String[] splitted = received.split(";");
        String status = splitted[1];
        if (status.equals("success")) {
            List<String> listUsername = new ArrayList<>();
            for (int i = 2; i < splitted.length; i++) {
                listUsername.add(splitted[i]);
            }
            StartClient.chatController.setListUser(listUsername);
        }  
    }

    private void onReceiveChatRoom(String received) {
        String[] splitted = received.split(";");
        ChatItem c = new ChatItem(splitted[1], splitted[2], splitted[3]);
        StartClient.chatController.addChat(c);
    }

    private void onReceiveLogout(String received) {
        this.loginUsername = null;
        StartClient.openScene(StartClient.SceneName.LOGIN);
        StartClient.closeScene(StartClient.SceneName.CHATROOM);

    }
}
