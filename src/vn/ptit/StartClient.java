/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vn.ptit;

import vn.ptit.controller.ChatController;
import vn.ptit.controller.ConnectServerController;
import vn.ptit.controller.LoginController;
import vn.ptit.controller.SocketHandler;
import vn.ptit.view.ChatView;
import vn.ptit.view.ConnectServerView;
import vn.ptit.view.LoginView;

/**
 *
 * @author Cuong Pham
 */
public class StartClient {
    public static SocketHandler socketHandler;
    public static ConnectServerView connectServerView;
    public static LoginView loginView;
    public static ChatView chatView;
    public static ChatController chatController;
    
    public enum SceneName {
        CONNECTSERVER,
        CHATROOM,
        LOGIN,
    }

    public StartClient() {
        socketHandler = new SocketHandler();
        openScene(SceneName.CONNECTSERVER);
    }
    
    public static void openScene(SceneName sceneName) {
        switch (sceneName) {
            case CONNECTSERVER:
                connectServerView = new ConnectServerView();
                ConnectServerController connectServerController = new ConnectServerController(connectServerView);
                connectServerView.setVisible(true);
                break;
            case LOGIN:
                loginView = new LoginView();
                LoginController loginController = new LoginController(loginView);
                loginView.setVisible(true);
                break;
            case CHATROOM:
                chatView = new ChatView();
                chatController = new ChatController(chatView);
                chatView.setVisible(true);
                break;

        }
    }

    public static void closeScene(SceneName sceneName) {
        switch (sceneName) {
            case CONNECTSERVER:
                connectServerView.dispose();
                break;
            case LOGIN:
                loginView.dispose();
                break;
            case CHATROOM:
                chatView.dispose();
                break;
        }
    }
    
    
    public static void main(String[] args) {
        new StartClient();
    }
    
}
